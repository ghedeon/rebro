package com.vv.rebro.util;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.util.messages.MessageBus;
import com.intellij.util.messages.MessageBusConnection;
import com.intellij.util.messages.Topic;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class EventUtil {

    public static <Handler> void subscribe(@Nullable final Project project, @Nullable Disposable parentDisposable, @NotNull final Topic<Handler> topic, final Handler handler) {
        if (project != null) {
            final MessageBusConnection messageBusConnection = connect(project, parentDisposable);
            messageBusConnection.subscribe(topic, handler);
        }
    }

    public static <Handler> void subscribe(@NotNull final Project project, @NotNull final Topic topic, @NotNull final Handler handler) {
        project.getMessageBus().connect().subscribe(topic, handler);
    }

    @NotNull
    public static <Listener> Listener post(@NotNull final Project project, @NotNull final Topic<Listener> topic) {
        final MessageBus messageBus = project.getMessageBus();

        return messageBus.syncPublisher(topic);
    }

    @NotNull
    private static MessageBusConnection connect(@Nullable final Disposable parentDisposable) {
        final MessageBus messageBus = ApplicationManager.getApplication().getMessageBus();

        return parentDisposable == null ? messageBus.connect() : messageBus.connect(parentDisposable);
    }

    @NotNull
    private static MessageBusConnection connect(@NotNull final Project project, @Nullable final Disposable parentDisposable) {
        final MessageBus messageBus = project.getMessageBus();

        return parentDisposable == null ? messageBus.connect(project) : messageBus.connect(parentDisposable);
    }
}
