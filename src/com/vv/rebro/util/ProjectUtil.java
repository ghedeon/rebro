package com.vv.rebro.util;

import com.intellij.ide.DataManager;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ProjectUtil {

    @NotNull
    public static Project fromComponent(@Nullable final Component component) {
        final DataContext dataContext = DataManager.getInstance().getDataContext(component);
        final Project project = DataKeys.PROJECT.getData(dataContext);

        return checkNotNull(project);
    }

}
