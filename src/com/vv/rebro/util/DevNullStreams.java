package com.vv.rebro.util;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class DevNullStreams {

    @NotNull
    public static final OutputStream OUTPUT_STREAM = new OutputStream() {
        public void write(int i) throws IOException {
        }
    };

    @NotNull
    public static final InputStream INPUT_STREAM = new InputStream() {
        public int read() throws IOException {
            return 0;
        }
    };

}
