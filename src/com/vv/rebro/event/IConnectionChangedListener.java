package com.vv.rebro.event;

import com.intellij.util.messages.Topic;
import org.jetbrains.annotations.NotNull;

import java.util.EventListener;

public interface IConnectionChangedListener extends EventListener {

    Topic<IConnectionChangedListener> TOPIC = Topic.create("DeviceConnection changed", IConnectionChangedListener.class);

    void onConnectionChanged(@NotNull String connectionId);

}