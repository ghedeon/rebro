package com.vv.rebro.event;

import com.intellij.util.messages.Topic;
import org.jetbrains.annotations.NotNull;

import java.util.EventListener;

public interface IConnectionDeletedListener extends EventListener {

    Topic<IConnectionDeletedListener> TOPIC = Topic.create("Device connection deleted", IConnectionDeletedListener.class);

    void onConnectionDeleted(@NotNull String connectionId);
}
