package com.vv.rebro.event;

import com.intellij.util.messages.Topic;
import org.jetbrains.annotations.NotNull;

import java.util.EventListener;

public interface IDeviceAddedListener extends EventListener {

    Topic<IDeviceAddedListener> TOPIC = Topic.create("Device connection added", IDeviceAddedListener.class);

    void onDeviceAdded(@NotNull String connectionId);
}
