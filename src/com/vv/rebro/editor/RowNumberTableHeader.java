package com.vv.rebro.editor;

import com.intellij.openapi.editor.colors.EditorColors;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.ui.table.JBTable;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;

class RowNumberTableHeader extends JBTable {

    RowNumberTableHeader(@NotNull final JTable table) {
        setModel(new RowNumberHeaderModel(table));

        setShowGrid(false);
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setPreferredScrollableViewportSize(new Dimension(30, 0));
        getColumnModel().getColumn(0).setPreferredWidth(30);
        getEmptyText().setText("");
        final EditorColorsScheme globalScheme = EditorColorsManager.getInstance().getGlobalScheme();
        final Color lineNumbersColor = globalScheme.getColor(EditorColors.LINE_NUMBERS_COLOR);
        setForeground(lineNumbersColor);
    }

    private static class RowNumberHeaderModel extends AbstractTableModel {

        @NotNull
        private JTable table;

        RowNumberHeaderModel(@NotNull final JTable table) {
            this.table = table;
        }

        @Override
        public int getColumnCount() {
            return 1;
        }

        @Override
        public Object getValueAt(int row, int column) {
            return table.convertRowIndexToModel(row) + 1;
        }

        @Override
        public int getRowCount() {
            return table.getRowCount();
        }
    }
}
