package com.vv.rebro.editor;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorPolicy;
import com.intellij.openapi.fileEditor.FileEditorProvider;
import com.intellij.openapi.fileEditor.FileEditorState;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.vv.rebro.file.RealmVirtualFile;
import com.vv.rebro.util.PluginXml;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;

@PluginXml
public class RealmEditorProvider implements FileEditorProvider, ApplicationComponent {

    private static final String COMPONENT_NAME = "RealmEditor";

    @Override
    public void initComponent() {
        //empty
    }

    @Override
    public void disposeComponent() {
        // empty
    }

    @NotNull
    @Override
    public String getComponentName() {
        return COMPONENT_NAME;
    }

    @Override
    public boolean accept(@NotNull final Project project, @NotNull final VirtualFile file) {
        return file instanceof RealmVirtualFile;
    }

    @NotNull
    @Override
    public FileEditor createEditor(@NotNull Project project, @NotNull VirtualFile file) {
        final RealmVirtualFile realmFile = (RealmVirtualFile) file;
        return new RealmEditor(project, realmFile);
    }

    @Override
    public void disposeEditor(@NotNull FileEditor editor) {

    }

    @NotNull
    @Override
    public FileEditorState readState(@NotNull Element sourceElement, @NotNull Project project, @NotNull VirtualFile file) {
        return null;
    }

    @Override
    public void writeState(@NotNull FileEditorState state, @NotNull Project project, @NotNull Element targetElement) {

    }

    @NotNull
    @Override
    public String getEditorTypeId() {
        return "dummy";
    }

    @NotNull
    @Override
    public FileEditorPolicy getPolicy() {
        return FileEditorPolicy.HIDE_DEFAULT_EDITOR;
    }
}
