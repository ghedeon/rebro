package com.vv.rebro.editor;

import com.vv.rebro.model.RTable;
import com.vv.rebro.model.RType;
import org.jetbrains.annotations.NotNull;

import javax.swing.table.AbstractTableModel;

class RealmTableModel extends AbstractTableModel {

    @NotNull
    private final RTable table;

    RealmTableModel(@NotNull final RTable table) {
        this.table = table;
    }

    @Override
    public int getRowCount() {
        return table.getValues()[0].length;
    }

    @Override
    public int getColumnCount() {
        return table.getColumnNames().length;
    }

    @Override
    public String getColumnName(int column) {
        return table.getColumnNames()[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (table.getColumnType(columnIndex) == RType.OBJECT) {
            return "(" + table.getColumnClass(columnIndex) + ")";
        }
        
        return table.getValueAt(columnIndex, rowIndex);
    }
}
