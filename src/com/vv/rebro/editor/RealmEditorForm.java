package com.vv.rebro.editor;

import com.intellij.openapi.editor.colors.EditorColors;
import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.openapi.project.Project;
import com.vv.rebro.connection.ConnectionManager;
import com.vv.rebro.connection.DeviceConnection;
import com.vv.rebro.connection.RealmTableRef;
import com.vv.rebro.file.RealmFileSystem;
import com.vv.rebro.model.RTable;
import com.vv.rebro.model.RType;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.hyperlink.AbstractHyperlinkAction;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.HyperlinkProvider;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class RealmEditorForm {

    @NotNull
    private final Project project;
    private JPanel jMainPanel;
    private JXTable jContentTable;
    private JScrollPane jScrollPane;
    @NotNull
    private RealmTableModel realmTableModel;

    RealmEditorForm(@NotNull final Project project, @NotNull final RealmTableRef tableRef) {
        this.project = project;
        final DeviceConnection connection = ConnectionManager.getInstance(project).getConnection(tableRef.getConnectionId());

        final RTable table = connection.getTable(tableRef.getTableName());
        realmTableModel = new RealmTableModel(table);

        jContentTable.setModel(realmTableModel);
        for (int columnIdx = 0; columnIdx < table.getColumnCount(); columnIdx++) {
            final RType columnType = table.getColumnType(columnIdx);
            if (columnType == RType.OBJECT) {
                jContentTable.getColumnExt(columnIdx).setEditable(false);
                jContentTable.getColumnExt(columnIdx).setCellRenderer(new DefaultTableRenderer(new HyperlinkProvider(new LinkAction(table, connection))));
            }
        }

        final EditorColorsScheme globalScheme = EditorColorsManager.getInstance().getGlobalScheme();
        jContentTable.setBackground(globalScheme.getColor(EditorColors.CARET_ROW_COLOR));

        final RowNumberTableHeader rowNumberTableHeader = new RowNumberTableHeader(jContentTable);
        jScrollPane.setRowHeaderView(rowNumberTableHeader);
    }

    @NotNull
    JPanel getComponent() {
        return jMainPanel;
    }

    void highlightRow(final int highlightRow) {
        jContentTable.setRowSelectionInterval(highlightRow, highlightRow);
    }

    void setTableRef(@NotNull final RealmTableRef tableRef) {
        final DeviceConnection connection = ConnectionManager.getInstance(project).getConnection(tableRef.getConnectionId());

        final RTable table = connection.getTable(tableRef.getTableName());
        realmTableModel = new RealmTableModel(table);


        jContentTable.setModel(realmTableModel);
        for (int columnIdx = 0; columnIdx < table.getColumnCount(); columnIdx++) {
            final RType columnType = table.getColumnType(columnIdx);
            if (columnType == RType.OBJECT) {
                jContentTable.getColumnExt(columnIdx).setEditable(false);
                jContentTable.getColumnExt(columnIdx).setCellRenderer(new DefaultTableRenderer(new HyperlinkProvider(new LinkAction(table, connection))));
            }
        }

        final EditorColorsScheme globalScheme = EditorColorsManager.getInstance().getGlobalScheme();
        jContentTable.setBackground(globalScheme.getColor(EditorColors.CARET_ROW_COLOR));

        final RowNumberTableHeader rowNumberTableHeader = new RowNumberTableHeader(jContentTable);
        jScrollPane.setRowHeaderView(rowNumberTableHeader);
    }

    private class LinkAction extends AbstractHyperlinkAction<Object> {

        private RTable table;
        private DeviceConnection connection;

        LinkAction(final RTable table, final DeviceConnection connection) {
            this.table = table;
            this.connection = connection;
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            final String columnClass = table.getColumnClass(jContentTable.getSelectedColumn());
            for (final RTable rTable : connection.getData()) {
                if (rTable.getName().equals(columnClass)) {
                    final int objectPointer = (int) table.getValueAt(jContentTable.getSelectedColumn(), jContentTable.getSelectedRow());
                    final RealmTableRef jumpTableRef = new RealmTableRef(connection.getId(), rTable.getName());
                    RealmFileSystem.getInstance().openEditor(connection.getProject(), jumpTableRef, objectPointer);
                    break;
                }
            }
        }
    }


    /*public static void main(String[] args) {
        System.out.println("test");
        RealmEditorForm form = new RealmEditorForm(file.getTableRef());
        JPanel component = form.getComponent();
        JFrame jFrame = new JFrame();
        jFrame.setContentPane(component);
        jFrame.pack();
        jFrame.setVisible(true);
    }*/

}
