package com.vv.rebro.file;

import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerAdapter;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.vv.rebro.util.EventUtil;
import com.vv.rebro.util.PluginXml;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

//TODO: Do I need this?
@PluginXml
public class RealmFileManager extends AbstractProjectComponent {


    private List<RealmVirtualFile> openedFiles = new ArrayList<>(); //todo??

    private RealmFileManager(@NotNull final Project project) {
        super(project);
    }

    @NotNull
    public static RealmFileManager getInstance(@NotNull final Project project) {
        final RealmFileManager realmFileManager = project.getComponent(RealmFileManager.class);

        return checkNotNull(realmFileManager);
    }

    @Override
    public void projectOpened() { //todo??
        EventUtil.subscribe(myProject, FileEditorListener.TOPIC, new FileEditorManagerAdapter() {
            @Override
            public void fileOpened(@NotNull final FileEditorManager source, @NotNull final VirtualFile file) {
                if (file instanceof RealmVirtualFile) {
                    openedFiles.add(((RealmVirtualFile) file));
                }
            }

            @Override
            public void fileClosed(@NotNull final FileEditorManager source, @NotNull final VirtualFile file) {
                if (file instanceof RealmVirtualFile) {
                    openedFiles.remove(file);
                }
            }
        });
    }

    public boolean isFileOpened(@NotNull final RealmVirtualFile file) {
        return openedFiles.contains(file);
    }
}
