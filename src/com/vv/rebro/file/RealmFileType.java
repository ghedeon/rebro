package com.vv.rebro.file;

import com.intellij.openapi.fileTypes.ex.FakeFileType;
import com.intellij.openapi.vfs.VirtualFile;
import icons.Icons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

class RealmFileType extends FakeFileType {

    static final RealmFileType INSTANCE = new RealmFileType();
    private static final String REALM_FILE_TYPE_NAME = "Realm";
    private static final String REALM_FILE_TYPE_DESCRIPTION = "Realm file type";

    private RealmFileType() {
        //empty
    }

    @Override
    public boolean isMyFileType(@NotNull final VirtualFile file) {
        return file instanceof RealmVirtualFile;
    }

    @NotNull
    @Override
    public String getName() {
        return REALM_FILE_TYPE_NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return REALM_FILE_TYPE_DESCRIPTION;
    }

    @NotNull
    @Override
    public Icon getIcon() {
        return Icons.TABLE;
    }
}
