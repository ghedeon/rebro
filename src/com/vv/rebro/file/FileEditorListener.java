package com.vv.rebro.file;


import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.messages.Topic;
import org.jetbrains.annotations.NotNull;

import java.util.EventListener;

public interface FileEditorListener extends EventListener {

    Topic<FileEditorListener> TOPIC = Topic.create("FileEditor", FileEditorListener.class);

    void fileOpened(@NotNull FileEditorManager source, @NotNull VirtualFile file);

    void fileClosed(@NotNull FileEditorManager source, @NotNull VirtualFile file);

}
