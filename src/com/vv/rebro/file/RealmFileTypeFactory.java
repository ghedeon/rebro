package com.vv.rebro.file;

import com.intellij.openapi.fileTypes.FileTypeConsumer;
import com.intellij.openapi.fileTypes.FileTypeFactory;
import com.vv.rebro.util.PluginXml;
import org.jetbrains.annotations.NotNull;

@PluginXml
public class RealmFileTypeFactory extends FileTypeFactory {

    @Override
    public void createFileTypes(@NotNull final FileTypeConsumer consumer) {
        consumer.consume(RealmFileType.INSTANCE);
    }
}
