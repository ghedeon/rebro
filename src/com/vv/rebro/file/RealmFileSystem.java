package com.vv.rebro.file;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.openapi.vfs.ex.dummy.DummyFileSystem;
import com.vv.rebro.connection.RealmTableRef;
import com.vv.rebro.editor.RealmEditor;
import com.vv.rebro.util.PluginXml;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@PluginXml
public class RealmFileSystem extends DummyFileSystem {

    private static final String PROTOCOL = "realm";

    @NotNull
    private Map<RealmTableRef, RealmVirtualFile> filesCache = newHashMap();

    @NotNull
    public static RealmFileSystem getInstance() {
        return (RealmFileSystem) VirtualFileManager.getInstance().getFileSystem(PROTOCOL);
    }

    public void openEditor(@NotNull final Project project, @NotNull final RealmTableRef tableRef) {
        final RealmVirtualFile realmFile = findOrCreateRealmFile(tableRef);
        //todo: is it the best way of background task?
        ApplicationManager.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                FileEditorManager fileEditorManager = FileEditorManager.getInstance(project);
                fileEditorManager.openFile(realmFile, true);
            }
        });
    }

    public void openEditor(@NotNull final Project project, @NotNull final RealmTableRef tableRef, final int highlightRow) {
        final RealmVirtualFile realmFile = findOrCreateRealmFile(tableRef);
        ApplicationManager.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                FileEditorManager fileEditorManager = FileEditorManager.getInstance(project);
                final FileEditor[] fileEditors = fileEditorManager.openFile(realmFile, true);
                if (fileEditors.length > 0) {
                    final RealmEditor fileEditor = (RealmEditor) fileEditors[0];
                    fileEditor.highlightRow(highlightRow);
                }
            }
        });
    }

    @NotNull
    private RealmVirtualFile findOrCreateRealmFile(final @NotNull RealmTableRef tableRef) {
        RealmVirtualFile file = filesCache.get(tableRef);
        if (file == null) {
            file = new RealmVirtualFile(tableRef);
            filesCache.put(tableRef, file);
        }

        return file;
    }

    @NotNull
    @Override
    public String getProtocol() {
        return PROTOCOL;
    }

    @Override
    public void fireContentsChanged(final Object requestor, @NotNull final VirtualFile file, final long oldModificationStamp) {
        super.fireContentsChanged(requestor, file, oldModificationStamp); //todo ??
    }

}
