package com.vv.rebro.file;

import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileSystem;
import com.vv.rebro.connection.RealmTableRef;
import com.vv.rebro.util.DevNullStreams;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class RealmVirtualFile extends VirtualFile {

    private static final byte[] EMPTY_BYTE_CONTENT = new byte[0];

    private String path = "/"; //todo: ??
    private long time = 1; //todo ??

    @NotNull
    private final RealmTableRef tableRef;

    RealmVirtualFile(@NotNull final RealmTableRef tableRef) {
        this.tableRef = tableRef;
    }

    @NotNull
    public String getConnectionId() {
        return tableRef.getConnectionId();
    }

    @NotNull
    public RealmTableRef getTableRef() {
        return tableRef;
    }

    //todo
    /*public void setTableRef(final RTable tableRef) {
        this.tableRef = tableRef;
        ApplicationManager.getApplication().runWriteAction(new Runnable() {
            public void run() {
                final RealmFileSystem fileSystem = (RealmFileSystem) RealmVirtualFile.this.getFileSystem();
                fileSystem.fireContentsChanged(this, RealmVirtualFile.this, 0);
            }
        });
    }*/

    @NotNull
    @Override
    public String getName() {
        return tableRef.getTableName();
    }

    @NotNull
    @Override
    public VirtualFileSystem getFileSystem() {
        return RealmFileSystem.getInstance();
    }

    @NotNull
    @Override
    public String getPath() {
        return path;
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    @Override
    public boolean isValid() {
        //TODO
        return true;
    }

    @Override
    public VirtualFile getParent() {
        //TODO
        return null;
    }

    @Override
    public VirtualFile[] getChildren() {
        return VirtualFile.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public OutputStream getOutputStream(Object requestor, long newModificationStamp, long newTimeStamp) throws IOException {
        return DevNullStreams.OUTPUT_STREAM;
    }

    @NotNull
    @Override
    public byte[] contentsToByteArray() throws IOException {
//        return EMPTY_BYTE_CONTENT;
        return "Lorem ipsum".getBytes(); //todo
    }

    @Override
    public long getTimeStamp() {
        return 1; //todo
    }

    @Override
    public long getLength() {
        try {
            return contentsToByteArray().length;
        } catch (IOException e) {
            e.printStackTrace(); //todo
            return 0;
        }
    }

    @Override
    public void refresh(boolean asynchronous, boolean recursive, @Nullable Runnable postRunnable) {
        //empty
    }

    @Override
    public InputStream getInputStream() throws IOException {
//        return DevNullStreams.INPUT_STREAM;
        return new ByteArrayInputStream(contentsToByteArray()); //todo
    }

    @Override
    public long getModificationStamp() {
        return time; // TODO
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return RealmFileType.INSTANCE;
    }
}
