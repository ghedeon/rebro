package com.vv.rebro.model;

import org.jetbrains.annotations.NotNull;

public class RTable {

    private String name;
    private Object[][] values;

    private String[] columnNames;
    private RType[] columnTypes;
    private String[] columnRawTypes;
    private String[] columnClasses;

    @NotNull
    private String connectionId;

    public RTable() {
    }

    public RTable(String name, int columns, int rows) {
        this.name = name;
        values = new Object[columns][rows];
        columnNames = new String[columns];
        columnTypes = new RType[columns];
        columnClasses = new String[columns];
    }

//    private List<Column> columns;
//    public int getRowCount();
//    public int getColumnCount();
//    public Class<?> getColumnClass(int columnIndex);


    @NotNull
    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(@NotNull final String connectionId) {
        this.connectionId = connectionId;
    }

    public Object getValueAt(int column, int row) {
        return values[column][row];
    }

    public void setColumnNames(String[] strings) {
        columnNames = strings;
    }

    public void setValueAt(Object value, int column, int row) {
        values[column][row] = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object[][] getValues() {
        return values;
    }

    public void setValues(Object[][] values) {
        this.values = values;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public void setColumnType(int columnIdx, RType type) {
        columnTypes[columnIdx] = type;
    }

    public void setColumnName(int i, String columnName) {
        columnNames[i] = columnName;
    }

    public void setColumnClass(int columnIdx, String name) {
        columnClasses[columnIdx] = name;
    }

    public String getColumnClass(int columnIdx) {
        return columnClasses[columnIdx];
    }

    public RType getColumnType(int columnIdx) {
        return columnTypes[columnIdx];
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public String[] getColumnClasses() {
        return columnClasses;
    }

    public void setColumnClasses(final String[] columnClasses) {
        this.columnClasses = columnClasses;
    }

    public RType[] getColumnTypes() {
        return columnTypes;
    }

    public void setColumnTypes(final RType[] columnTypes) {
        this.columnTypes = columnTypes;
    }

    public String[] getColumnRawTypes() {
        return columnRawTypes;
    }

    public void setColumnRawTypes(final String[] columnRawTypes) {
        this.columnRawTypes = columnRawTypes;
        if (columnTypes == null) {
            columnTypes = new RType[columnRawTypes.length];
            for (int i = 0; i < columnRawTypes.length; i++) {
                columnTypes[i] = RType.valueOf(columnRawTypes[i]);
            }
        }
    }
}
