package com.vv.rebro.model;

public enum RType {
    INTEGER,
    BOOLEAN,
    STRING,
    BINARY,
    DATE,
    FLOAT,
    DOUBLE,
    OBJECT,
    LIST,
    UNSUPPORTED;

}
