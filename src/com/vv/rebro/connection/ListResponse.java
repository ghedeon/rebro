package com.vv.rebro.connection;

import com.vv.rebro.model.RTable;

import java.util.List;

public class ListResponse {

    private List<RTable> tables;
    private String connectionId;
    private String dbName;

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(final String connectionId) {
        this.connectionId = connectionId;
    }

    public List<RTable> getTables() {
        return tables;
    }

    public void setTables(final List<RTable> tables) {
        this.tables = tables;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(final String dbName) {
        this.dbName = dbName;
    }
}
