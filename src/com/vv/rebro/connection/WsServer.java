package com.vv.rebro.connection;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.thetransactioncompany.jsonrpc2.*;
import com.vv.rebro.model.RTable;
import net.minidev.json.JSONValue;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.*;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Lists.newArrayList;


class WsServer extends WebSocketServer {

    private static final String TAG = WsServer.class.getSimpleName();

    @NotNull
    private final ConnectionManager connectionManager;
    @NotNull
    private final BiMap<String, WebSocket> idToSocket = HashBiMap.create();

    private String ip;

    @NotNull
    static WsServer newInstance(@NotNull final ConnectionManager connectionManager) {
//         enable for verbose logging
//        WebSocketImpl.DEBUG = true;
        return new WsServer(connectionManager);
    }

    private WsServer(@NotNull final ConnectionManager connectionManager) {
        super(new InetSocketAddress(Config.SERVER_PORT));
        this.connectionManager = connectionManager;
    }

    @Override
    public void start() {
        super.start();
        final List<String> ips = discoverSelfIp();
        checkArgument(!ips.isEmpty(), "Failed to obtain server ip!");
        ip = ips.get(0);
    }

    @Override
    public void onOpen(@NotNull final WebSocket connection, @NotNull final ClientHandshake handshake) {
        System.out.println(TAG + ": new connection from " + connection.getRemoteSocketAddress().getAddress().getHostAddress());
    }

    @Override
    public void onClose(@NotNull final WebSocket connection, final int code, @NotNull final String reason, final boolean remote) {
        System.out.println(TAG + ": " + connection + " disconnected");
        final String connectionId = idToSocket.inverse().get(connection);
        idToSocket.remove(connectionId);
        connectionManager.onDisconnected(connectionId);
    }

    @Override
    public void onMessage(@NotNull final WebSocket connection, @NotNull final String message) {
        System.out.println(TAG + ": received (" + connection + "): " + message);
        try {
            final JSONRPC2Message jsonMessage = JSONRPC2Message.parse(message);
            if (jsonMessage instanceof JSONRPC2Request) {
                onHandleRequest(connection, ((JSONRPC2Request) jsonMessage));
            } else if (jsonMessage instanceof JSONRPC2Response) {
                onHandleResponse(connection, ((JSONRPC2Response) jsonMessage));
            } else if (jsonMessage instanceof JSONRPC2Notification) {
                onHandleNotification(connection, ((JSONRPC2Notification) jsonMessage));
            }
        } catch (JSONRPC2ParseException ex) {
            ex.printStackTrace();
            //todo handle exception
        }
    }

    private void onHandleRequest(@NotNull final WebSocket connection, @NotNull final JSONRPC2Request request) {
        final RpcMethod rpcMethod = RpcMethod.valueOf(request.getMethod());
        switch (rpcMethod) {
            case CONNECT:
                final String deviceName = (String) request.getNamedParams().get("DEVICE_NAME");
                final String connectionId = (String) request.getNamedParams().get("CONNECTION_ID");

                idToSocket.put(connectionId, connection);
                connectionManager.addNewConnection(connectionId, deviceName);

                final UUID requestId = UUID.randomUUID();
                final JSONRPC2Request listRequest = new JSONRPC2Request(RpcMethod.LIST.name(), requestId);
                connection.send(listRequest.toString());

                System.out.println(TAG + ": send: " + listRequest.toString());
                break;
            case LIST:
                break;
        }
    }

    private void onHandleResponse(@NotNull final WebSocket connection, @NotNull final JSONRPC2Response response) {
        if (response.indicatesSuccess()) {
            JSONValue.remapField(RTable.class, "columnTypes", "columnRawTypes"); //TODO better way?

            final ListResponse listResponse = JSONValue.parse(response.getResult().toString(), ListResponse.class);
            final String connectionId = listResponse.getConnectionId();
            final DeviceConnection deviceConnection = connectionManager.getConnection(connectionId);
            deviceConnection.setData(listResponse.getTables());
            deviceConnection.setDbName(listResponse.getDbName());
            connectionManager.notifyDataChanged(connectionId);
        }
    }

    private void onHandleNotification(@NotNull final WebSocket connection, @NotNull final JSONRPC2Notification notification) {
        final RpcMethod rpcMethod = RpcMethod.valueOf(notification.getMethod());
        switch (rpcMethod) {
            case PUSH:
                final ListResponse listResponse = JSONValue.parse(notification.getNamedParams().get("data").toString(), ListResponse.class);
                final DeviceConnection deviceConnection = connectionManager.getConnection(listResponse.getConnectionId());
                deviceConnection.setData(listResponse.getTables());
                connectionManager.notifyDataChanged(listResponse.getConnectionId());

                System.out.println(listResponse);
                break;
        }
    }

    @Override
    public void onError(@Nullable final WebSocket connection, @NotNull final Exception ex) {
        //TODO handle
        ex.printStackTrace();
        if (connection != null) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @NotNull
    private List<String> discoverSelfIp() {
        final List<String> ips = newArrayList();
        try {
            final Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                final NetworkInterface networkInterface = (NetworkInterface) interfaces.nextElement();
                if (!networkInterface.isLoopback() && networkInterface.isUp() && isWiFi(networkInterface.getName())) {
                    final Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
                    while (addresses.hasMoreElements()) {
                        final InetAddress address = addresses.nextElement();
                        if (address instanceof Inet4Address) {
                            ips.add(address.getHostAddress());
                            System.out.println(TAG + " wifi IP: " + address.getHostAddress());
                        }
                    }
                }
            }
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
/*        if (SystemInfo.isWindows) {
            try {
                final String cmd = "cmd /c for /f \"tokens=3\" %a in ('netsh interface ip show address \"Wi-Fi\" ^| findstr \"IP Address\"') do @echo %a";
                final Process process = Runtime.getRuntime().exec(cmd);
                final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String s;
                while ((s = reader.readLine()) != null) {
                    ip = s.trim();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            final GeneralCommandLine ipRouteCommand = new GeneralCommandLine("sh", "-c", "ip route | awk '/src/{print $9}'");
            ip = ExecUtil.execAndReadLine(ipRouteCommand);
        }*/

        return ips;
    }

    private static boolean isWiFi(@NotNull final String networkInterface) {
        return networkInterface.startsWith("wlan") || networkInterface.startsWith("wlp") || networkInterface.startsWith("en0");
    }

    @NotNull
    String getIp() {
        return ip;
    }

    void sendDisconnect(@NotNull final String connectionId) {
        final WebSocket webSocket = idToSocket.get(connectionId);
        if (webSocket != null) {
            final JSONRPC2Notification notification = new JSONRPC2Notification(RpcMethod.DISCONNECT.name());
            webSocket.send(notification.toString());
            System.out.println(TAG + ": send " + notification.toString());
        }
    }

}
