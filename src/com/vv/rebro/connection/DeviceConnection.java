package com.vv.rebro.connection;

import com.intellij.openapi.project.Project;
import com.vv.rebro.model.RTable;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class DeviceConnection {

    @NotNull
    private String id;
    @NotNull
    private String name = "";
    @NotNull
    private String dbName = "";
    @NotNull
    private List<RTable> data = new ArrayList<>(); //todo map?
    private Project project;

    public DeviceConnection() {
/*
        id = UUID.randomUUID().toString();

        final RTable table1 = new RTable("Table1", 2, 3);
        table1.setColumnNames(new String[]{"ColumnA", "ColumnB"});
        table1.setColumnType(0, RType.OBJECT);
        table1.setColumnType(1, RType.INTEGER);
        table1.setColumnClass(0, "Table2");
        table1.setColumnClass(1, "Integer");
        table1.setValueAt("Lorem", 0, 0);
        table1.setValueAt("Lorem", 0, 1);
        table1.setValueAt("Lorem", 0, 2);
        table1.setValueAt(1, 1, 0);
        table1.setValueAt(1, 1, 1);
        table1.setValueAt(1, 1, 2);
        table1.setConnectionId(this);


        final RTable table2 = new RTable("Table2", 2, 3);
        table2.setColumnNames(new String[]{"ColumnA", "ColumnB"});
        table2.setColumnType(0, RType.STRING);
        table2.setColumnType(1, RType.BOOLEAN);
        table2.setValueAt("Lorem", 0, 0);
        table2.setValueAt("Lorem", 0, 1);
        table2.setValueAt("Lorem", 0, 2);
        table2.setValueAt(true, 1, 0);
        table2.setValueAt(false, 1, 1);
        table2.setValueAt(true, 1, 2);
        table2.setConnectionId(this);

        data.add(table1);
        data.add(table2);
*/
    }

    public DeviceConnection(@NotNull final String id) {
        this.id = id;
    }


    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public List<RTable> getData() {
        return data;
    }

    public void setData(@NotNull final List<RTable> data) {
        this.data = data;
        for (final RTable rTable : data) {
            rTable.setConnectionId(id);
        }
    }

    public Project getProject() {
        return project;
    }

    public void setProject(final Project project) {
        this.project = project;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    @NotNull
    public RTable getTable(@NotNull final String tableName) {
        RTable result = null;
        for (final RTable table : data) {
            if (table.getName().equals(tableName)) {
                result = table;
            }
        }

        return checkNotNull(result, "Unknown table name!");
    }
}
