package com.vv.rebro.connection;

import com.google.common.base.Objects;
import org.jetbrains.annotations.NotNull;

public final class RealmTableRef {

    @NotNull
    private String connectionId;
    @NotNull
    private String tableName;

    public RealmTableRef(@NotNull final String connectionId, @NotNull final String tableName) {
        this.connectionId = connectionId;
        this.tableName = tableName;
    }

    @NotNull
    public String getConnectionId() {
        return connectionId;
    }

    @NotNull
    public String getTableName() {
        return tableName;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final RealmTableRef that = (RealmTableRef) o;
        return Objects.equal(connectionId, that.connectionId) &&
                Objects.equal(tableName, that.tableName);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(connectionId, tableName);
    }
}
