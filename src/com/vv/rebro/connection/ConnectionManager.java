package com.vv.rebro.connection;

import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.AppUIUtil;
import com.vv.rebro.event.IConnectionChangedListener;
import com.vv.rebro.event.IConnectionDeletedListener;
import com.vv.rebro.event.IDeviceAddedListener;
import com.vv.rebro.file.RealmVirtualFile;
import com.vv.rebro.util.EventUtil;
import com.vv.rebro.util.PluginXml;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

@PluginXml
public class ConnectionManager extends AbstractProjectComponent {

    @NotNull
    private Map<String, DeviceConnection> connections = newHashMap();
    private WsServer wsServer;
    @Nullable
    private String selectedConnectionId;

    protected ConnectionManager(@NotNull final Project project) {
        super(project);
    }

    @NotNull
    public static ConnectionManager getInstance(@NotNull final Project project) {
        final ConnectionManager connectionManager = project.getComponent(ConnectionManager.class);

        return checkNotNull(connectionManager);
    }

    @NotNull
    public synchronized DeviceConnection getConnection(@NotNull final String connectionId) {
        return connections.get(connectionId);
    }

    synchronized void addNewConnection(@NotNull final String connectionId, @NotNull final String deviceName) {
        final DeviceConnection deviceConnection = new DeviceConnection();
        deviceConnection.setId(connectionId);
        deviceConnection.setName(deviceName);
        deviceConnection.setProject(myProject);
        connections.put(connectionId, deviceConnection);
        AppUIUtil.invokeOnEdt(new Runnable() {
            @Override
            public void run() {
                EventUtil.post(myProject, IDeviceAddedListener.TOPIC).onDeviceAdded(connectionId);
            }
        });
    }

    void notifyDataChanged(@NotNull final String connectionId) {
        AppUIUtil.invokeOnEdt(new Runnable() {
            @Override
            public void run() {
                final FileEditorManager editorManager = FileEditorManager.getInstance(myProject);
                final VirtualFile[] openFiles = editorManager.getOpenFiles();
                for (final VirtualFile openFile : openFiles) {
                    if (openFile instanceof RealmVirtualFile) {
                        final RealmVirtualFile realmVirtualFile = (RealmVirtualFile) openFile;
                        if (realmVirtualFile.getConnectionId().equals(connectionId)) { //TODO
/*                            final DeviceConnection deviceConnection = connections.get(connectionId);
                            for (final RTable rTable : deviceConnection.getData()) {
                                if (rTable.getName().equals(realmVirtualFile.getTableRef().getName())) {
                                    realmVirtualFile.setTableRef(rTable);
                                    break;
                                }
                            }*/
                        }


//                        editorManager.closeFile(openFile);
//                        editorManager.openFile(openFile, false);
/*
                        final FileEditor[] editors = editorManager.getEditors(openFile);
                        for (final FileEditor editor : editors) {
                            ((RealmEditor) editor).
                        }*/
//                        editorManager.openFile(openFile, false);
                    }
                }
                EventUtil.post(myProject, IConnectionChangedListener.TOPIC).onConnectionChanged(connectionId);
            }
        });
    }

    @Override
    public void projectOpened() {
        startServer();
    }

    @Override
    public void projectClosed() {
        try {
            stopServer();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace(); //todo handle
        }
    }

    @NotNull
    public String getServerIp() {
        return wsServer.getIp();
    }

    private void startServer() {
        // todo check autobahn, wamp, jsonrpc from jetbrains, jsonrpc4j
        wsServer = WsServer.newInstance(this);
        wsServer.start();

        System.out.println("WsServer started: " + wsServer.getIp() + ":" + wsServer.getPort());
    }

    private void stopServer() throws IOException, InterruptedException {
        wsServer.stop();
    }

    @Nullable
    public String getSelectedConnectionId() {
        return selectedConnectionId;
    }

    public void setSelectedConnectionId(@Nullable final String selectedConnectionId) {
        this.selectedConnectionId = selectedConnectionId;
    }

    public void deleteConnection(@NotNull final String connectionId) {
        connections.remove(connectionId);
        wsServer.sendDisconnect(connectionId); //todo async?
        AppUIUtil.invokeOnEdt(new Runnable() {
            @Override
            public void run() {
                EventUtil.post(myProject, IConnectionDeletedListener.TOPIC).onConnectionDeleted(connectionId);
                final FileEditorManager editorManager = FileEditorManager.getInstance(myProject);
                final VirtualFile[] openFiles = editorManager.getOpenFiles();
                for (final VirtualFile openFile : openFiles) {
                    if (openFile instanceof RealmVirtualFile) {
                        final RealmVirtualFile realmVirtualFile = (RealmVirtualFile) openFile;
                        if (realmVirtualFile.getConnectionId().equals(connectionId)) {
                            editorManager.closeFile(openFile);
                        }
                    }
                }
            }
        });
    }

    void onDisconnected(@NotNull final String connectionId) {
        //todo
    }
}
