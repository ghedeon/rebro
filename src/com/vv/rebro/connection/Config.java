package com.vv.rebro.connection;

public class Config {

    public static final int SERVER_PORT = 8887;
    public static final String BROADCAST_ADDRESS_NAME = "255.255.255.255";
    public static final String DISCOVERY_REQUEST = "discoveryRequest";
    public static final String DISCOVERY_RESPONSE = "discoveryResponse";
}