package com.vv.rebro.connection;

enum RpcMethod {
    CONNECT,
    LIST,
    PUSH,
    DISCONNECT
}
