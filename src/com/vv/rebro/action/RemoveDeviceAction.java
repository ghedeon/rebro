package com.vv.rebro.action;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import com.vv.rebro.connection.ConnectionManager;
import com.vv.rebro.util.PluginXml;
import org.jetbrains.annotations.NotNull;

@PluginXml
public class RemoveDeviceAction extends DumbAwareAction {

    @Override
    public void actionPerformed(@NotNull final AnActionEvent event) {
        final Project project = event.getProject();
        if (project == null) {
            return;
        }

        final ConnectionManager connectionManager = ConnectionManager.getInstance(project);
        final String selectedConnectionId = connectionManager.getSelectedConnectionId();
        if (selectedConnectionId != null) {
            connectionManager.deleteConnection(selectedConnectionId);
        }
    }
}
