package com.vv.rebro.action;

import com.android.ddmlib.*;
import com.android.tools.idea.run.DeviceCount;
import com.android.tools.idea.run.DeviceSelectionUtils;
import com.android.tools.idea.run.TargetDeviceFilter;
import com.intellij.execution.RunManager;
import com.intellij.execution.RunnerAndConfigurationSettings;
import com.intellij.execution.configurations.ModuleBasedConfiguration;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import com.vv.rebro.connection.ConnectionManager;
import com.vv.rebro.util.PluginXml;
import org.jetbrains.android.facet.AndroidFacet;
import org.jetbrains.android.sdk.AndroidSdkUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Collection;

@PluginXml
public class AddDeviceAction extends DumbAwareAction {

    @Override
    public void actionPerformed(@NotNull final AnActionEvent event) {
        final Project project = event.getProject();
        if (project != null) {
            AndroidFacet androidFacet = null;
            final Module[] modules = ModuleManager.getInstance(project).getModules();
            for (final Module module : modules) {
                final AndroidFacet facet = AndroidFacet.getInstance(module);
                if (facet != null) {
                    androidFacet = facet;
                    break;
                }
            }

            if (androidFacet == null) {
                System.out.println("Android Facet not found"); //TODO Notification
                return;
            }

            AndroidSdkUtils.activateDdmsIfNecessary(project);
            final Collection<IDevice> devices = DeviceSelectionUtils.chooseRunningDevice(androidFacet, new TargetDeviceFilter() {
                @Override
                public boolean matchesDevice(@NotNull final IDevice iDevice) {
                    return true;
                }
            }, DeviceCount
                    .SINGLE);
            if (devices == null || devices.size() == 0) {
                System.out.println("No devices found"); //TODO notification
                return;
            }

            System.out.println(devices);
            for (final IDevice device : devices) {
                String serverIp;
                if (device.isEmulator()) {
                    serverIp = "10.0.2.2";
                } else {
                    final ConnectionManager connectionManager = ConnectionManager.getInstance(project);
                    serverIp = connectionManager.getServerIp();
                }

                try {
                    device.executeShellCommand("am broadcast -a com.vv.rebro.action.CONNECT -e SERVER_IP " + serverIp + " -e DEVICE_NAME " + device.getSerialNumber(), new NullOutputReceiver());
                } catch (TimeoutException e) {
                    e.printStackTrace();
                } catch (AdbCommandRejectedException e) {
                    e.printStackTrace();
                } catch (ShellCommandUnresponsiveException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }



            /*final DeviceChooserDialog deviceChooserDialog = new DeviceChooserDialog(facet, facet.getConfiguration()
                    .getAndroidTarget(), false, null, null);

            deviceChooserDialog.setTitle("Select Device for Realm connection");
            deviceChooserDialog.show();
            if (deviceChooserDialog.isOK()) {
                final IDevice[] selectedDevices = deviceChooserDialog.getSelectedDevices();
            }*/

//            ConnectionManager.getInstance(project).addNewConnection();
        }
    }

    /**
     * Returns the active module from the selected Run Configuration.
     *
     * @return {@link Module}
     */
    @Nullable
    private Module getModule(Project project) {

        final RunManager runManager = RunManager.getInstance(project);
        final RunnerAndConfigurationSettings configurationSettings = runManager.getSelectedConfiguration();
        if (configurationSettings == null) {
//            showNotification("Run Configuration is not defined", NotificationType.ERROR);
            return null;
        }
        final ModuleBasedConfiguration selectedConfiguration = (ModuleBasedConfiguration) configurationSettings.getConfiguration();
        final Module module = selectedConfiguration.getConfigurationModule().getModule();
        if (module == null) {
//            showNotification("Module is not specified for selected Run Configuration", NotificationType.ERROR);
        }

        return module;
    }
}
