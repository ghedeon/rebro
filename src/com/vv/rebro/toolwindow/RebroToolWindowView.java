package com.vv.rebro.toolwindow;

import com.intellij.openapi.actionSystem.ActionGroup;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.actionSystem.impl.ActionToolbarImpl;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.ui.TabbedPaneImpl;
import com.intellij.ui.components.JBTabbedPane;
import com.vv.rebro.connection.ConnectionManager;
import com.vv.rebro.event.IConnectionChangedListener;
import com.vv.rebro.event.IConnectionDeletedListener;
import com.vv.rebro.event.IDeviceAddedListener;
import com.vv.rebro.util.EventUtil;
import com.vv.rebro.util.PluginXml;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@PluginXml
public class RebroToolWindowView {

    private JPanel jMainPanel;
    private JBTabbedPane jTabbedPane;
    private ActionToolbarImpl jToolbar;

    @NotNull
    private final ConnectionManager connectionManager;
    @NotNull
    private final Map<String, RebroToolWindowTab> connectionToTab = newHashMap();
    @NotNull
    private final Map<Component, RebroToolWindowTab> componentToTab = newHashMap();

    @NotNull
    static RebroToolWindowView getInstance(@NotNull final Project project) {
        return ServiceManager.getService(project, RebroToolWindowView.class);
    }

    RebroToolWindowView(@NotNull final Project project, @NotNull final ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        EventUtil.subscribe(project, IDeviceAddedListener.TOPIC, new DeviceAddedListener());
        EventUtil.subscribe(project, IConnectionDeletedListener.TOPIC, new ConnectionDeletedListener());
        EventUtil.subscribe(project, IConnectionChangedListener.TOPIC, new ConnectionChangedListener());
    }

    @NotNull
    JPanel getComponent() {
        return jMainPanel;
    }

    private void createUIComponents() {
        createToolbarUI();
        createTabsUI();
    }

    private void createToolbarUI() {
        final ActionManager actionManager = ActionManager.getInstance();
        final ActionGroup controlsActionGroup = (ActionGroup) actionManager.getAction("Rebro.ActionGroup.ToolWindow.Controls");
        final ActionToolbar toolbar = actionManager.createActionToolbar("", controlsActionGroup, true);
        toolbar.setTargetComponent(jMainPanel);
        jToolbar = (ActionToolbarImpl) toolbar;
    }

    private void createTabsUI() {
        jTabbedPane = new TabbedPaneImpl(SwingConstants.TOP);
        jTabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(final ChangeEvent e) {
                final boolean hasNoConnections = jTabbedPane.getSelectedComponent() == null;
                if (hasNoConnections) {
                    connectionManager.setSelectedConnectionId(null);
                } else {
                    final RebroToolWindowTab selectedTab = componentToTab.get(jTabbedPane.getSelectedComponent());
                    connectionManager.setSelectedConnectionId(selectedTab.getConnectionId());
                }
            }
        });
    }

    private class DeviceAddedListener implements IDeviceAddedListener {

        @Override
        public void onDeviceAdded(@NotNull final String connectionId) {
            final RebroToolWindowTab tab = new RebroToolWindowTab(connectionManager, connectionId);
            componentToTab.put(tab.getComponent(), tab);
            connectionToTab.put(connectionId, tab);

            jTabbedPane.addTab(tab.getName(), tab.getComponent());
            final int lastIdx = jTabbedPane.getTabCount() - 1;
            jTabbedPane.setSelectedIndex(lastIdx);
        }
    }

    private class ConnectionChangedListener implements IConnectionChangedListener {

        @Override
        public void onConnectionChanged(@NotNull final String connectionId) {
            final RebroToolWindowTab tab = connectionToTab.get(connectionId);
            tab.update();
        }
    }

    private class ConnectionDeletedListener implements IConnectionDeletedListener {

        @Override
        public void onConnectionDeleted(@NotNull final String connectionId) {
            final JPanel component = connectionToTab.get(connectionId).getComponent();
            jTabbedPane.remove(component);
            connectionToTab.remove(connectionId);
            componentToTab.remove(component);
        }
    }


/*    public static void main(String[] args) {
        RebroToolWindowView form = new RebroToolWindowView();
        JPanel component = form.getComponent();
        JFrame jFrame = new JFrame();
        jFrame.setContentPane(component);
        jFrame.pack();
        jFrame.setVisible(true);
    }*/
}
