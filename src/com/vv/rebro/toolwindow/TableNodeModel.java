package com.vv.rebro.toolwindow;

import com.vv.rebro.connection.RealmTableRef;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

final class TableNodeModel extends NodeModel {

    @NotNull
    private final RealmTableRef table;

    TableNodeModel(@NotNull final RealmTableRef tableRef, @NotNull final Icon icon) {
        super(tableRef.getTableName(), icon);
        this.table = tableRef;
    }

    @NotNull
    RealmTableRef getTableRef() {
        return table;
    }
}
