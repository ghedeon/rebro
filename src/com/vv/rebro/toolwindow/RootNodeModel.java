package com.vv.rebro.toolwindow;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class RootNodeModel extends NodeModel {

    public RootNodeModel(@NotNull final String name, @NotNull final Icon icon) {
        super(name, icon);
    }
}
