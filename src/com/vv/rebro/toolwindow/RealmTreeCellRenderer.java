package com.vv.rebro.toolwindow;

import com.intellij.ui.ColoredTreeCellRenderer;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

class RealmTreeCellRenderer extends ColoredTreeCellRenderer {

    @Override
    public void customizeCellRenderer(@NotNull JTree jTree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        DefaultMutableTreeNode value1 = (DefaultMutableTreeNode) value;
        NodeModel nodeModel = (NodeModel) value1.getUserObject();
        Icon icon = nodeModel.getIcon();
        setIcon(icon);
        append(nodeModel.getName());
    }
}