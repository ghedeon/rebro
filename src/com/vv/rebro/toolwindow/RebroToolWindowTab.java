package com.vv.rebro.toolwindow;

import com.intellij.openapi.project.Project;
import com.intellij.ui.treeStructure.Tree;
import com.intellij.util.ui.tree.TreeUtil;
import com.vv.rebro.connection.ConnectionManager;
import com.vv.rebro.connection.DeviceConnection;
import com.vv.rebro.connection.RealmTableRef;
import com.vv.rebro.file.RealmFileSystem;
import com.vv.rebro.model.RTable;
import icons.Icons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static com.vv.rebro.util.ProjectUtil.fromComponent;

class RebroToolWindowTab {

    private static final String TABLES_NODE_NAME = "Tables";
    private JPanel mainPanel;
    private Tree tree;

    @NotNull
    private final ConnectionManager connectionManager;
    @NotNull
    private final String connectionId;
    private DeviceConnection deviceConnection;

    RebroToolWindowTab(@NotNull final ConnectionManager connectionManager, @NotNull final String connectionId) {
        this.connectionManager = connectionManager;
        this.connectionId = connectionId;
    }

    /**
     * generated
     */
    private void createUIComponents() {
        deviceConnection = connectionManager.getConnection(connectionId);
        tree = new Tree(createRootModel());
        tree.setCellRenderer(new RealmTreeCellRenderer());
        tree.setExpandsSelectedPaths(true);
        tree.addMouseListener(new RebroToolWindowTab.TreeMouseAdapter());
        TreeUtil.expandAll(tree);
    }

    @NotNull
    JPanel getComponent() {
        return mainPanel;
    }

    @NotNull
    String getName() {
        return deviceConnection.getName();
    }

    @NotNull
    String getConnectionId() {
        return connectionId;
    }

    void update() {
        tree.setModel(createRootModel());
        TreeUtil.expandAll(tree);
    }

    private DefaultTreeModel createRootModel() {
        final DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(new RootNodeModel(deviceConnection.getName(), Icons.CONNECTION));

        final int tableCount = deviceConnection.getData().size();
        final DefaultMutableTreeNode tablesNode = new DefaultMutableTreeNode(
                new NodeModel(TABLES_NODE_NAME + " (" + tableCount + ")", Icons.TABLES));

        for (RTable table : deviceConnection.getData()) {
            final RealmTableRef tableRef = new RealmTableRef(connectionId, table.getName());
            final DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(new TableNodeModel(tableRef, Icons.TABLE));
            tablesNode.add(treeNode);
        }
        rootNode.add(tablesNode);

        return new DefaultTreeModel(rootNode);
    }

    private class TreeMouseAdapter extends MouseAdapter {

        @Override
        public void mouseClicked(@NotNull final MouseEvent event) {
            if (isDoubleClick(event)) {
                final Project project = fromComponent(event.getComponent());
                if (project == null) {
                    return;
                }

                final TreePath path = tree.getPathForLocation(event.getX(), event.getY());
                if (path != null) {
                    final Object nodeModel = ((DefaultMutableTreeNode) path.getLastPathComponent()).getUserObject();
                    if (nodeModel instanceof TableNodeModel) {
                        RealmFileSystem.getInstance().openEditor(project, ((TableNodeModel) nodeModel).getTableRef());
                    }
                }

                event.consume();
            }
        }

        private boolean isDoubleClick(@NotNull final MouseEvent event) {
            return event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 2;
        }

    }


    /*public static void main(String[] args) {
        RebroToolWindowTab form = new RebroToolWindowTab();
        JPanel component = form.getComponent();
        JFrame jFrame = new JFrame();
        jFrame.setContentPane(component);
        jFrame.pack();
        jFrame.setVisible(true);
    }*/
}
