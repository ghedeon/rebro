package com.vv.rebro.toolwindow;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;


public class NodeModel {

    @NotNull
    protected final String name;
    @NotNull
    protected final Icon icon;

    public NodeModel(@NotNull final String name, @NotNull final Icon icon) {
        this.name = name;
        this.icon = icon;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public Icon getIcon() {
        return icon;
    }

}
