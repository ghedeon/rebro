package com.vv.rebro.toolwindow;

import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.vv.rebro.util.PluginXml;
import icons.Icons;
import org.jetbrains.annotations.NotNull;

@PluginXml
public class RebroToolWindowFactory implements ToolWindowFactory, DumbAware {

    @Override
    public void createToolWindowContent(@NotNull final Project project, @NotNull final ToolWindow toolWindow) {
        final RebroToolWindowView toolWindowView = RebroToolWindowView.getInstance(project);
        final ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        final Content content = contentFactory.createContent(toolWindowView.getComponent(), null, false);

        toolWindow.getContentManager().addContent(content);
        toolWindow.setIcon(Icons.TOOL_WINDOW);
    }
}
