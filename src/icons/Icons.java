package icons;

import com.intellij.openapi.util.IconLoader;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Don't move it from this package.
 *
 * @see <a href=http://www.jetbrains.org/intellij/sdk/docs/reference_guide/work_with_icons_and_images.html>
 * http://www.jetbrains.org/intellij/sdk/docs/reference_guide/work_with_icons_and_images.html
 * </a>
 */
public final class Icons {

    public static final Icon TOOL_WINDOW = load("/icons/DatabaseBrowser.png");
    public static final Icon TABLES = load("/icons/Tables.png");
    public static final Icon TABLE = load("/icons/Table.png");
    public static final Icon CONNECTION = load("/icons/ConnectionActive.png");
    public static final Icon ADD = load("/icons/Add.png");
    public static final Icon REMOVE = load("/icons/Remove.png");

    @NotNull
    private static Icon load(@NotNull final String path) {
        return IconLoader.getIcon(path);
    }

}
